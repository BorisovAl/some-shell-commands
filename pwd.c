#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char * argv[]) {
    if (argv[1] && *(argv[1]) == '-' && strcmp(argv[1], "-L") != 0 && strcmp(argv[1], "-P") != 0) 
	{
        perror("ERROR: Invalid Syntax");
        exit(1);
    }
    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    printf("%s\n", cwd);
}
