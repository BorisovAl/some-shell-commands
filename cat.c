#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main(int argc, char * argv[]) {
    int firstFileNum = 1;
    int isCommandWithFlag = 0;
    int j = 0;
    if (!argv[1]) {
        printf("Error: Too few arguments");
        exit(0);
    }
    else if ( *(argv[1]) == '-') {
        if (strcmp(argv[1], "-n") == 0) 
		{
			j = 1;
            firstFileNum++;
            isCommandWithFlag = 1;
        }
        else 
		{
            printf("Error. Invalid Syntax");
            exit(0);
        }
    }
    
    int lineNumber = 1;
    if ((j==1)&&(argc == 2))
    while(1)
    {
	}
    for (int i = firstFileNum; i < argc; i++) {
        FILE* fp;
        fp = fopen(argv[i], "r");
        if (fp == NULL) 
		{
            printf("Error. Couldn't open file");
            exit(0);
        }
        
        char* str = NULL;
        ssize_t read;
        size_t len = 0;
        
        while ((read = getline(&str, &len, fp)) != -1) {
            if (isCommandWithFlag)
                printf("     %d  %s", lineNumber, str);
            else
                printf("%s", str);
            
            lineNumber++;
        }
        
        fclose(fp);
    }
}
