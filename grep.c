#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 1024 

void grep (FILE *fd,char *str,int f);

int main (int argc, char *argv[]) {
  if ((argc != 3) && (argc != 4)) {
    printf("Error:Wrong number of arguments\n");
    return 1;
  }

  if (argc == 3) { 
    FILE *fp = fopen(argv[2],"r");
    if (fp == NULL) {
      printf("Error:Can't open %s\n",argv[2]);
      return 1;
    }
    char *str = argv[1];
    grep(fp,str,0);
    fclose(fp);

  } else {

    FILE *fp = fopen(argv[3],"r");
    if (fp == NULL) {
      printf("Error:Can't open %s\n",argv[2]);
      return 1;
    }
    char *str = argv[2];
    grep(fp,str,1);
    fclose(fp);
  }
  return 0;
}


void grep (FILE *fp,char *str,int v) {
  char *fline = malloc(MAX);
  int len = strlen(str);
  int i;

  if (v) { 
    while (fgets(fline,MAX,fp)) {
      i = 0;
      while (fline[i] != '\0') {
        if (memcmp(fline+i,str,len) == 0)
          break;
        ++i;
      }
      if (fline[i] == '\0')
        printf("%s",fline);
    }

  } else { 

    while (fgets(fline,MAX,fp)) {
      i = 0;
      while (fline[i] != '\0')
        if (memcmp(fline+i,str,len) == 0) {
          printf("%s",fline);
          break;
        } else {
          ++i;
        }
    }
  }
}
