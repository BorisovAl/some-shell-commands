#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

int isDirectory(const char *path) {
    struct stat statbuf;
    if (lstat(path, &statbuf) != 0)
        return 0;
    return S_ISDIR(statbuf.st_mode) && !S_ISLNK(statbuf.st_mode);
}

void printPlain(char* directory) {
    struct dirent **namelist;
    int n = scandir(directory, &namelist, NULL, alphasort);
    
    int printedObjects = 0;
    
    for (int i = 0; i < n; i++) {
        if (*(namelist[i]->d_name) != '.') {
            printf("%s   ",namelist[i]->d_name);
            printedObjects++;
        }
        free(namelist[i]);
    }
    if (printedObjects)
        printf("\n");
    free(namelist);
}

void printRecursively(char* directory) {
    printPlain(directory);
    
    struct dirent **namelist;
    int n = scandir(directory, &namelist, NULL, alphasort);
    
    for (int i = 0; i < n; i++) {
        char* innerDirectory = malloc(strlen(directory) + 1 + strlen(namelist[i]->d_name));
        strcpy(innerDirectory, directory);
        strcat(innerDirectory, "/");
        strcat(innerDirectory, namelist[i]->d_name);
        
        if (
            *(namelist[i]->d_name) != '.' &&
            isDirectory(innerDirectory)
            ) {
            printf("\n%s:\n", innerDirectory);
            printRecursively(innerDirectory);
        }
        free(namelist[i]);
        free(innerDirectory);
    }
    free(namelist);
}

void printWithAttributes(char* directory) {
    struct dirent **namelist;
    int n = scandir(directory, &namelist, NULL, alphasort);
    
    for (int i = 0; i < n; i++) {
        if (*(namelist[i]->d_name) != '.') {
            struct stat fileStat;
            lstat(namelist[i]->d_name, &fileStat);
            
            printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
            printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
            printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
            printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
            printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
            printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
            printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
            printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
            printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
            printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
            
            printf("   %ld", fileStat.st_nlink);
            
            struct passwd *fileOwner = getpwuid(fileStat.st_uid);
            printf("   %s", fileOwner->pw_name);
            
            struct group *fileGroup = getgrgid(fileStat.st_gid);
            printf("   %s", fileGroup->gr_name);
            
            printf("   %5ld", fileStat.st_size);
            
            char date[13];
            strftime(date, 13, "%b %d %H:%M", localtime(&(fileStat.st_ctime)));
            printf("   %s", date);
            
            printf("   %s", namelist[i]->d_name);
            
            if (S_ISLNK(fileStat.st_mode)) {
                char buf[1024];
                ssize_t len = readlink(namelist[i]->d_name, buf, sizeof(buf)-1);
                buf[len] = '\0';
                printf(" -> %s", buf);
            }
            
            printf("\n");
        }
        free(namelist[i]);
    }
    free(namelist);
}

void printWithUserGroup(char* directory) {
    struct dirent **namelist;
    int n = scandir(directory, &namelist, NULL, alphasort);
    
    for (int i = 0; i < n; i++) {
        if (*(namelist[i]->d_name) != '.') {
            struct stat fileStat;
            stat(namelist[i]->d_name, &fileStat);
            
            struct passwd *fileOwner = getpwuid(fileStat.st_uid);
            printf("%s", fileOwner->pw_name);
            
            struct group *fileGroup = getgrgid(fileStat.st_gid);
            printf("   %s", fileGroup->gr_name);
            
            printf("   %s\n", namelist[i]->d_name);
        }
        free(namelist[i]);
    }
    free(namelist);
}

int main(int argc, char* argv[]) {
    if (argv[1] && *(argv[1]) == '-') {
        char* directory = ".";
        if (argc == 3) {
            directory = argv[2];
        }

        if (strcmp(argv[1], "-R") == 0)
            printRecursively(directory);
        else if (strcmp(argv[1], "-l") == 0)
            printWithAttributes(directory);
        else if (strcmp(argv[1], "-g") == 0)
            printWithUserGroup(directory);
        else {
            perror("Error: Invalid Syntax");
            exit(1);
        }
    } else {
        char* directory = ".";
        if (argc == 2) {
            directory = argv[1];
        }

        printPlain(directory);
    }
}
