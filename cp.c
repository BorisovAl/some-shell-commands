#include <stdio.h> 
#include <fcntl.h> 
#include <sys/types.h> 
#include <sys/stat.h>
#include <unistd.h>

enum {

    BUFSIZE = 1024
};

int main(int argc, char * argv[]){

    struct stat ss1, ss2;
    stat(argv[1], &ss1);
    stat(argv[2], &ss2);
    if (ss1.st_mode == ss2.st_mode       &&
        ss1.st_dev == ss2.st_dev         &&
        ss1.st_ino == ss2.st_ino         &&
        ss1.st_nlink == ss2.st_nlink     &&
        ss1.st_uid == ss2.st_uid         &&
        ss1.st_gid == ss2.st_gid         &&
        ss1.st_rdev == ss2.st_rdev       &&
        ss1.st_size == ss2.st_size       &&
        ss1.st_atime == ss2.st_atime     &&
        ss1.st_mtime == ss2.st_mtime     &&
        ss1.st_ctime == ss2.st_ctime     &&
        ss1.st_blksize == ss2.st_blksize 
        ) {
            
            printf("The same files");
            return 0;
          }

    char buf[BUFSIZE];
    int fd1, fd2, cn;
     
    if ((fd1 = open(argv[1],O_RDONLY)) == -1) {
        printf("Error opening file\n");
        return 0; 
    }

    if ((fd2 = creat(argv[2], ss1.st_mode)) == -1) {
    
        printf("Error creating file\n");
        return 0; 
    }

    while ((cn = read(fd1, &buf, sizeof(buf))) > 0) 
        if (write(fd2, buf, cn) != cn) {
         
            printf("Error writing to file\n");
            return 0;
        }
}
