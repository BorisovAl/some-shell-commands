#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[]) {
    int areSlashSymbolsEnabled = 0;
    int isNewLine = 1;

    int startIndex = 1;

    for (startIndex = 1; startIndex < argc; startIndex++) {
        if (strcmp(argv[startIndex], "-e") == 0)
            areSlashSymbolsEnabled = 1;
        else if (strcmp(argv[startIndex], "-E") == 0)
            areSlashSymbolsEnabled = 0;
        else if (strcmp(argv[startIndex], "-n") == 0)
            isNewLine = 0;
        else break;
    }

    if (areSlashSymbolsEnabled) {
        for (int i = startIndex; i < argc; i++) {
            for (int j = 0; j < strlen(argv[i]); j++) {
                if (*(argv[i]+j) == '\\') {
                    switch (*(argv[i]+j+1)) {
                        case 'n':
                            printf("\n");
                            break;
                        case 't':
                            printf("\t");
                            break;
                        default:
                            break;
                    }
                    j++;
                }
                else {
                    printf("%c", *(argv[i]+j));
                }
            }
            printf(" ");
        }
    }
    else {
        for (int i = startIndex; i < argc; i++)
            printf("%s ", argv[i]);
    }

    if (isNewLine)
        printf("\n");
}
